#Responsive Crafty.js game with Laravel PHP Framework back-end

Codename: Vallu & Valma 2 is a carreer inspirational game with roleplay game aspect.
The characted starts at the end of high school and proceeds by making choices about his/her
education, carreer and/or hobbies. These will effect the outcome. Currently only one characted per player.

## Feature Overview

- Laravel powered login
- Upon logging in the Javascript front-end
- Ajax requests to the PHP from Javascript (XML/JSON)
- Database to hold users, their characters and character stats and the messages they receive.
- Placeholder graphics.

## A Few Examples

### Ajax call to back-end  for updating LVL:

```php
<?php
Route::post('user/sam/add/xp', array('uses' => 'user@lvled'));
```
Route is accessed by the javascript by an AJAX reguest, which  issued as a static function in the game.
In Crafty.js static functions are introduced as components (Crafty.c):
```Javascript
Crafty.c("PostAjax", {
    Ajax: function(address) {
    if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlHttp = new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
//Luodaan selaimen mukaan muuttuja xmlhttp-pyynnön iilmentymää varten.
    xmlHttp.open("POST", address, false);
    xmlHttp.send();
} // Ajax-funktio - Post ei palauta mitään joten PlaceJax ja raportti ovat tarpeettomia funktioita.
}); // Ajax Post-komponentti
```
And summoned by creating entities:
```javascript
Crafty.e("PostAjax")
.Ajax("user/sam/add/voitto");
```
## License

[Laravel](http://www.laravel.com/ "Laravel - PHP Framework for Ẃeb Artisans") is an open-source software licensed under the MIT License.
[Crafty.js](http://www.craftyjs.com/ "Code like a fox") is opensource and licensed under MIT or GPL license.
V2 game is copyrighted by to Aron Elal under CC-BY-SA

#Project Bones

##Project Assets Management

The project repo is hosted on [bitbucket](http://www.bitbucket.org/ "Source Version Control") and the project resources and assets management is on [Teambox](http://www.teambox.com/ "Manage your project assets and communicate with all participants"). The integration of the afformentioned is acçomplished with [Zapier](http://zapier.com/ "Automate the web.").

