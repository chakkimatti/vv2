window.onload = function(e) {

//prevent default browser behaviour on onload (the game)
e.preventDefault();

// Event options depending on Touch support of the agent
    var xmlHttp = null;
    var WIDTH = window.innerWidth;
    var HEIGHT = window.innerHeight;
    var a = 0; //bubblecount laskuri
    var m = 0; //viestihudin bind/unbind laskuri
    if (WIDTH >= 960)
    {
        WIDTH = 960;
        HEIGHT = 768;
       Crafty.mobile = false;
    }
    if (WIDTH<=800 && WIDTH>480)
            {
        WIDTH = 768;
        HEIGHT = 576;
       Crafty.mobile = true;
    }
    else if (WIDTH<=480)
    {
    WIDTH = 480;
    HEIGHT = 320;
    Crafty.mobile = true;
    }
    Crafty.init(WIDTH, HEIGHT); 
    var asratio = WIDTH/960;
    var aspath = WIDTH +"/";
var IMG_UNMUTE = "img/" +aspath +"play.png",
    IMG_MUTE = "img/" +aspath+ "unplay.png",
    IMG_XP = "img/" +aspath+ "xp.png",
    IMG_DARA = "img/" +aspath+ "taala.png",
    IMG_INT = "img/" +aspath+ "int.png",
    IMG_STR = "img/" +aspath+ "str.png",
    IMG_SKY = "img/" +aspath +"city.png",
    IMG_WTHR = "img/" +aspath +"vuorokausi.png",
    IMG_CLOUD = "img/" +aspath +"taivas.jpg"
    IMG_GRND = "img/" +aspath +"maa.png",
    IMG_KOULU = "img/" +aspath +"koulu.png",
    IMG_AREENA = "img/" +aspath +"areena.png",
    IMG_TUOKKARI = "img/" +aspath +"tuokkari.png",
    IMG_TUOKKARIS = "img/" +aspath +"tuokkaris.png",
    IMG_TUOPAIKKA = "img/" +aspath +"tuopaikka.png",
    IMG_X = "img/" +aspath +"x.png",
    IMG_POINT = "img/" +aspath +"point.png",
    VV2_OGG = "audio/vv2.ogg",
    VV2_MP3 = "audio/vv2.mp3",
    VV2_WAV = "audio/vv2.wav",
    
    //Lataus-skene
    Crafty.scene("loading", function () {
        //Ladataan taustabiisi
        Crafty.audio.stop();


        //Ladataan mitä tarvitaan ja..
//Musanappula
Crafty.c("Jukebox", {
init: function() {
Crafty.audio.add( "BGmusic", [VV2_MP3, VV2_WAV, VV2_OGG]);
this.requires ("2D, DOM, Mouse, Image, Persist")
.image(IMG_UNMUTE)
.attr({w:asratio*50, h: asratio*50, x: WIDTH-asratio*140, y: asratio*10, z: 999})
.css({   "cursor" : "hand", "cursor" : "pointer" ,
            "border-radius" :  "2px",
            "-moz-border-radius" : "2px",
            "-webkit-border-radius" : "2px"
            })
.bind("lefted", function () { 
                if(this.x<WIDTH)
               { this.x += WIDTH+1;} 
                })

.bind("righted", function () { 
                if(this.x > WIDTH)
               { this.x -= WIDTH+1;} })   //seuraa mukana liikuttaessa
.bind("Click", function() {

    Crafty.audio.toggleMute("BGmusic");
    if(this.__image === IMG_UNMUTE)
        {

            this.image(IMG_MUTE);
            return this;}
else if(this.__image === IMG_MUTE)
        {this.image(IMG_UNMUTE);
            return this;}
//toggle the mute/unmute icon

    }) // Click bind
    .bind("exited", function() {
        this.destroy();
        return this;

    });
return this;
}
}); //Crafty.c("Jukebox")
//Musanappula koko pelin ajan näkyvissä - paitti sit menus ku mennään pois pelist. Musarki loppuu
        Crafty.load([IMG_UNMUTE,
        IMG_MUTE,
        IMG_INT,
        IMG_STR,
        IMG_DARA,
        IMG_XP,
        IMG_SKY,
        IMG_WTHR,
        IMG_CLOUD,
        IMG_GRND,
        IMG_KOULU,
        IMG_AREENA,
        IMG_TUOKKARI,
        IMG_TUOKKARIS,
        IMG_TUOPAIKKA,
        IMG_POINT,
        VV2_WAV,
        VV2_MP3,
        VV2_OGG], function () {
            Crafty.scene("city"); //.. siirrytään peliin, kun on valmista
        });
//Harmaa tausta ja latausteksti..
        Crafty.background("#131313");
        Crafty.e("2D, DOM, Text").attr({ w: WIDTH, h: HEIGHT, x: this._w/2, y: 0 })
        .text("Ladataan..")
        .css({ "text-align": "center", "padding-top" : "160px", "font-size" : " 30px" });
        }); // Crafty.scene("Loading")
    Crafty.c("msgs_get",  {
        init: function() {
        if (m==0)
        {
         m = 1;
        Crafty.e("GetAjax")
        .Ajax("user/sam/messages")
        .list();
        Crafty.e("2D, DOM, Color, Mouse")
        .color("#000")
        .attr({w:WIDTH, h: HEIGHT, z: 800, x: 0, y:0, alpha: 0.4})
        .bind("Click", function() {
        Crafty.trigger("msgs_down");
        this.destroy();
        m = 0;
            })  

         } // Ajax kysely, JOS ikkuna on auki

        return this;
         //...KUN viesti-ikkuna suljetaan --> msgs_down event

      } // init
    }); // Crafty.c msgs_get


    /* ---------- Minipeli ----------------
- Ladataan mySprite.png
- Aloitetaan peli luomalla tason objektit
- Peli loppuu kun liekit on "sammuttettu."
-------------------------------------- */
        Crafty.scene("initGame", function() {
        Crafty.viewport.x =0;
        Crafty.viewport.mouselook();
        Crafty.e("slide")
        .unsetit();
        string = "";
        var counter = null;
        var sprite = "img/" +aspath +"mySprite.png";
        Crafty.sprite(asratio*64, sprite, {
            grass1: [0, 0],
            grass2: [1, 0],
            grass3: [2, 0],
            grass4: [3, 0],
            flame: [0, 1],
             bush1: [0, 2],
            bush2: [1, 2],
            banana: [4, 0],
            player: [0,4],
            empty: [1,4]
    });

        //generoidaan kartta
    function generateWorld() {
        //loop through all tiles
        var tilex =  Math.round(WIDTH/(asratio*64));
        var tiley =  Math.round(HEIGHT/(asratio*64));
        
        for (var i = 0; i <tilex; i++) { // 960(tai alle) / 64
            for (var j = 0; j < tiley; j++) { // 520 (tai alle) / 64

                //place grass on all tiles
                grassType = Crafty.math.randomInt(1, 4);
                Crafty.e("2D, DOM, grass" + grassType)
                    .attr({ x: i * (asratio*64), y: j * (asratio*64), z: 5}); 
                //grid of bushes
                if( (i !=0) && (i % 5 === 0) && (j % 3 === 0) ) {
                    Crafty.e("2D, DOM, solid, bush1")
                        .attr({x: i * (asratio*64), y: j * (asratio*64), z: 6})
                }

                //create a fence of bushes
                if(i === 0 && i == WIDTH-asratio*64 || j === 0 || j == HEIGHT-asratio*64)
                    Crafty.e("2D, DOM, solid, bush" + Crafty.math.randomInt(1, 2))
                    .attr({ x: i * (asratio*64), y: j * (asratio*64), z: 6 }); 

                //generate some flames within the boundaries of the outer bushes
                if (i > 2 && i < WIDTH/(asratio*64) && j > 3 && j < HEIGHT/(asratio*64)
                        && Crafty.math.randomInt(0, 40) > 36)
                         {
                    var f = Crafty.e("2D, DOM, flame, Collision, solid, explodable")
                            .attr({ x: i * (asratio*64), y: j * (asratio* 64), z: 66})
                            .bind('explode', function() {
                                this.destroy();
                                counter--;
                                Crafty.trigger("point");
                            })
                            .collision();
                            counter++;
                    if(f.hit('solid')) {
                        f.destroy();
                        }

                } //flame maker 
            }
        }
    } //generateWorld

Crafty.scene("loadGame", function () {
/*-----Minipelin komponentteja---
- Määritellään tarvittavat komponentit pelaajan luontiin, 
- hahmon animointiin, kontrollointiin ja "ammusvalmiuteen"
-------------------------------------------------------- */

Crafty.c("1up", function () {
    this.requires("2D, DOM, Image, Tween, Grid")
    .image(IMG_POINT)
    .attr({ w: 60, h:60, z:999, alpha: 1})
    .css({"color" : "#000", "font-size" : "50px", "text-align" : "center"})
    .tween({alpha: -5, y: this._y+180}, 1400);
});

Crafty.c("LeftControls", {
init: function() {
    this.requires('Fourway');
},

leftControls: function(speed) {
    this.fourway(speed, {W: -90, S: 90, D: 0, A: 180})
    return this;
}
}); // Crafty.c leftControls

Crafty.c("Ape", {
Ape: function() {
    //setup animations
    this.requires("SpriteAnimation, Collision")
    .animate("walk_left", 6, 3, 8)
    .animate("walk_right", 9, 3, 11)
    .animate("walk_up", 3, 3, 5)
    .animate("walk_down", 0, 3, 2)
    //change direction when a direction change event is received
    .bind("NewDirection",
        function (direction) {
            if (direction.x < 0) {
                if (!this.isPlaying("walk_left"))
                    this.stop().animate("walk_left", 10, -1);
            }
            if (direction.x > 0) {
                if (!this.isPlaying("walk_right"))
                    this.stop().animate("walk_right", 10, -1);
            }
            if (direction.y < 0) {
                if (!this.isPlaying("walk_up"))
                    this.stop().animate("walk_up", 10, -1);
            }
            if (direction.y > 0) {
                if (!this.isPlaying("walk_down"))
                    this.stop().animate("walk_down", 10, -1);
            }
            if(!direction.x && !direction.y) {
                this.stop();
            }
    })
    // A rudimentary way to prevent the user from passing solid areas
    .bind('Moved', function(from) {
        if(this.hit('solid')){
            this.attr({x: from.x, y:from.y});
        }
    }).onHit("fire", function() {
        this.destroy();
        Crafty.scene("dead");
        //play sound 'AAARGH!!!''
    });
return this;
}
}); // Crafty.c Ape => .Ape()

Crafty.c('Grid', {
    _cellSize: asratio*64,
    Grid: function(cellSize) {
        if(cellSize) this._cellSize = cellSize;
        return this;
    },
    col: function(col) {
        if(arguments.length === 1) {
            this.x = this._cellSize * col;
            return this;
        } else {
            return Math.round(this.x / this._cellSize);
        }
    },
    row: function(row) {
        if(arguments.length === 1) {
            this.y = this._cellSize * row;
            return this;
        } else {
            return Math.round(this.y / this._cellSize);
        }
    },
    snap: function(){
        this.x = Math.round(this.x/this._cellSize) * this._cellSize;
        this.y = Math.round(this.y/this._cellSize) * this._cellSize;
    }
}); //Crafty.c Grid

Crafty.c('BombDropper', {
    _dropped: 0,
    maxBombs: 1,
    _key: Crafty.keys.SPACE,

    init: function() {
        var dropper = this;
        this.requires('Grid')

        //Create the bomb
        .bind('KeyDown', function(e) {
            if (e.key !== this._key) {
                return;
            }
            
            if(this._dropped < this.maxBombs) {
                Crafty.e('BananaBomb')
                    .attr({z:100})
                    .col(this.col())
                    .row(this.row())
                    .BananaBomb()
                    .bind('explode', function() {
                        dropper._dropped--;
                        Crafty.trigger("SubBomb");

                        Crafty.trigger("AddBomb");
                        });


                this._dropped++
            }
        });
    },
    bombDropper: function(key) {
        this._key = key;
        return this;
    }
}); /* Crafty.c Bombdropper - "komponennti ammusvalmiuteen"
    Tämä komponentti luo BananaBomb entiteetin, jonka komponenntikuvaus tulee nyt */

Crafty.c('BananaBomb', {

    init: function() {
        this.requires("2D, DOM, SpriteAnimation, Grid, banana, explodable")
            .animate('explode', 4, 0, 5)
            .animate('explode', 50, -1)
            .timeout(function() {
                this.trigger("explode");
            }, 3000)
            .bind('explode', function() {
                this.destroy();
                //Create fire from the explosion
                for(var i = this.col() - 2; i < this.col()+3; i++)
                    {Crafty.e("BananaFire").attr({ z:800 }).col(i).row(this.row());}
                for(var i = this.row() - 2; i < this.row()+3; i++)
                    {Crafty.e("BananaFire").attr({ z:800 }).col(this.col()).row(i);}
            });
    },

    BananaBomb: function() {
        //Create shadow fire to help the AI
        for(var i = this.col() - 2; i < this.col()+3; i++)
            Crafty.e("ShadowBananaFire").attr({ z:9000 }).col(i).row(this.row())
        for(var i = this.row() - 2; i < this.row()+3; i++)
            Crafty.e("ShadowBananaFire").attr({ z:9000 }).col(this.col()).row(i)
        return this;
    }
}); // Crafty.c BananaBomb - requires explodable komponentin, luo BananaFire entiteetin

Crafty.c('BananaFire', {

    init: function() {
        this.requires("2D, DOM, SpriteAnimation, banana, Grid, Collision, fire")
            .animate('fire', 4, 0, 5)
            .animate('fire', 10, -1)
            .collision()
            .onHit('explodable', function(o) {
                        Crafty.e("2D, DOM, Image, Tween, Grid")
                        .image(IMG_POINT)
                        .attr({ w: asratio*64, h:asratio*64, z:999, alpha: 1})
                        .tween({alpha: -5, y: this.y-880}, 600)
                        .col(this.col())
                        .row(this.row())
                for(var i = 0; i < o.length; i++) {
                    o[i].obj.trigger("explode");

                    return this;
                }
            })
            .timeout(function() {
                this.destroy();
            }, 2000);
    }
}); //Crafty.c BananaFire

    // Helps the AI avoid unsafe tiles. Created when a bomb is dropped and removed after fire is gone
Crafty.c('ShadowBananaFire', {

    init: function() {
        this.requires("2D, Grid, empty, Collision, ShadowFire")
            .collision()
            .timeout(function() {
                this.destroy();
            }, 6100);
    }
});

Crafty.c('Grid', {
    _cellSize: asratio*64,
    Grid: function(cellSize) {
        if(cellSize) this._cellSize = cellSize;
        return this;
    },
    col: function(col) {
        if(arguments.length === 1) {
            this.x = this._cellSize * col;
            return this;
        } else {
            return Math.round(this.x / this._cellSize);
        }
    },
    row: function(row) {
        if(arguments.length === 1) {
            this.y = this._cellSize * row;
            return this;
        } else {
            return Math.round(this.y / this._cellSize);
        }
    },
    snap: function(){
        this.x = Math.round(this.x/this._cellSize) * this._cellSize;
        this.y = Math.round(this.y/this._cellSize) * this._cellSize;
    }
});

        //ladataan taas tavaraa
        Crafty.load([sprite], function () {
            Crafty.scene("main"); 
}); // Crafty.scene loadGame

        Crafty.background("#808080");
        Crafty.e("2D, DOM, Text").attr({ w: 100, h: 20, x: 150, y: 120 })
                .text("Ladataan..")
                .css({ "text-align": "center" });

                                
    });




//minipelin aloitus - scene
Crafty.scene("main", function () {
    generateWorld();
            // Luodaan pelaajahahmon ilmentymä em. komponenttien avulla
        var player1 = Crafty.e("2D, DOM, Ape, player, LeftControls, BombDropper, Collision")
                .attr({ x: asratio*64, y: HEIGHT-asratio*128, z: 9 })
                .leftControls(asratio*4)
                .onHit("collectable", function(){Crafty.trigger("collected");})
                .Ape();
                
        if(player1.y>HEIGHT-asratio*64)
        {Crafty.viewport.scroll('_y', HEIGHT);}                
        
 
        
        Crafty.e("2D, DOM, HTML, Color")
         .bind("point", function () { 

                if(counter===0)
                {Crafty.scene("win");}
                })
                .css({ "text-align" : "center", "padding-top" : "30px"});

        
});
// Ja täst se lähtee - taas
    Crafty.scene("loadGame"); //

    Crafty.scene ("dead", function() {
        Crafty.background("#808080");
        Crafty.e("2D, DOM, Text").attr({ w: 200, h: 20, x: WIDTH/2-100, y: HEIGHT/2-40 })
                .text("You're dead, amigo!..")
                .css({ "text-align": "center", "font-size" : "40px" });
                Crafty.e("2D, DOM, Mouse")
                .attr({x: 0, y: 0, w: WIDTH, h: HEIGHT, z: 3})
        .bind("Click",function() {
            Crafty.scene("initGame");
                    
                });
    }); // dead scene
        Crafty.scene("win", function() {
        Crafty.e("2D, DOM, Color")
        .color("#2356ba")
        .attr({x: 0, y: 0, w: WIDTH, h: HEIGHT, z:1}); // Tausta
        Crafty.e("2D, DOM, Image, Mouse")
        .attr({x: WIDTH/2-asratio*100, y: HEIGHT/2-asratio*100, w: asratio*100, h: asratio*100, z: 999})
        .image(IMG_XP)
         .css ({ "cursor" : "hand", "cursor" : "pointer" ,"-webkit-user-select" : "none",
        "-moz-user-select" : "none" })
        .bind("Click",function() {
            Crafty.e("PostAjax")
            .Ajax("user/sam/add/xp");
            Crafty.scene("loading");     
                }); //vasemmanpuoleinen, XP nappi
        Crafty.e("2D, DOM, Image, Mouse")
        .attr({x: WIDTH/2+asratio* 100, y: HEIGHT/2-asratio*100, w: asratio*100, h: asratio*100, z: 999})
        .image(IMG_DARA)
        .css ({ "cursor" : "hand", "cursor" : "pointer" ,"-webkit-user-select" : "none",
        "-moz-user-select" : "none" })
        .bind("Click",function() {
            Crafty.e("PostAjax")
            .Ajax("user/sam/add/bling");
            Crafty.scene("loading");
            
        }); //oikeanpuoleinen RAHA nappi

                Crafty.trigger("updateHud");
                

    }); // win scene

    }); // Crafty.scene initGame
/* ---------- Työkkäri-näkymä ----------------
- koko näytön taustakuva
- TODO: klikkauspistekartta
- TODO: toiminnallisuus
- Takaisin kaupunkinäkymään
-------------------------------------- */

/* ---------- Varsinainen peli ----------------
- Aloitusvalikko
- Kaupunkinäkymä
- Siirtymä ensimmäiseen minipeliin, jonka
jälkeen palataan pelin kaupunkinäkymään
-------------------------------------- */
   Crafty.scene("menu", function() {
    //Esitellään globaali komponentti pelin ensimmäisessä scenessä - loogista?
    Crafty.viewport.x = 0;
    Crafty("Jukebox").destroy();

    Crafty.c("GetAjax", {
    Ajax: function(address) {
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlHttp = new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
//...Eli luodaan selaimen mukaan muuttuja xmlhttp-pyynnön iilmentymää varten.
    xmlHttp.open("GET", address, false);
    xmlHttp.send();
    var report = xmlHttp.responseText;
    this._objectdata = JSON.parse(report);
    return this;
    },
    tester: function() {
        this._objectdata ? this._truey = true : this._truey = false;
        return this;
    },
    flush: function() {
        string = "";
        return this;
    },
    read: function(address) {
       if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlHttp = new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
//...Eli luodaan selaimen mukaan muuttuja xmlhttp-pyynnön iilmentymää varten.

    xmlHttp.open("GET", address, false);
    xmlHttp.send();
    this._objectdata = xmlHttp.responseXML;
    console.log(this._objectdata);

    },
    placeJax: function() {
        this.requires("2D,HTML");
            for (var i in this._objectdata)
        {string += +this._objectdata[i] +"<br/>";}
        this.append(string);
        return this;
    },
    readMsg: function() {
        this.requires("2D,HTML, counter");
            for (var i in this._objectdata)
        {string += "<p>" +this._objectdata[this._counter].original.id +"</>p";}
        this.append(string);
        return this;
    },
    count: function() {
        this.requires("Mouse, DOM, HTML")
            .bind("lefted", function () { 
                if(this.x<WIDTH)
               { this.x += WIDTH+1;} 
                })

            .bind("righted", function () { 
                if(this.x > WIDTH)
               { this.x -= WIDTH+1;} })   //seuraa mukana liikuttaessa
        .css ({ "cursor" : "hand", "cursor" : "pointer" ,"-webkit-user-select" : "none",
        "-moz-user-select" : "none" })
        if(this._objectdata[this._objectdata.length-1].count!=0)
        {
        this.append("Sinulla on " +this._objectdata[this._objectdata.length-1].count // length alkaa ykkösestä, kun array numerointi alkaa nollasta.
            +" uutta viestiä! " )
        .bind("Click", function() {
            Crafty.e("msgs_get");
        }); //bind
        return this;
    } //jos on viestejä
    else {
        this.append("Sinulla ei ole yhtään uutta viestiä! " )
        return this;} // Samanlainen ilmoitus, jos ei viestejä
    }, // count-function 

    list: function() {
            
            for (var c=0;c<this._objectdata.length-1;c++)
                /* length-1 on objektit miinus viimeinen Count objekti*/
        {
            Crafty.c("counter", {
                init: function() {
                    this._counter = c;
                    return this;
                }
            });

        Crafty.e("Mouse, 2D, DOM, Color")
        .attr({y: ([c+1]*94), x: 70, w: 800, h: 90, z: 900, alpha: 1})
        .css({ "cursor" : "hand", "cursor" : "pointer" ,
        "-moz-border-radius" : "20px",
        "-webkit-border-radius" : "20px",
        "border-radius" : "20px",
        "border" : "2px solid black",
        "-webkit-user-select" : "none",
        "-moz-user-select" : "none"
        }) 
        .color("#ffffff")
        .bind("msgs_down", function() {
            this.destroy();
        }); 
        
        Crafty.e("2D, DOM, HTML")
        .append(this._objectdata[c].original.id +") <u>" +this._objectdata[c].original.from +"</u><br/>"
        +"to: " +this._objectdata[c].original.to +"<br/>"
        +"<p>" +this._objectdata[c].original.message +"</p>")
        .attr({y: ([c+1]*94), x: 75, w: 800, h: 80, z: 998, alpha: 1})
        .css({"cursor" : "hand", "cursor" : "pointer", "color" : "red", "padding-top" : "10px", "webkit-user-select" : "none"})
        .bind("msgs_down", function () { 
        this.destroy();
        }); //bind all entities
        } //luodaan entiteettejä jonossa.

        return this;
    }, //listing messages function
    egobar: function() {
        Crafty.e("2D, DOM, HTML");
this.bind("lefted", function () { 
        if(this.x<WIDTH)
       { this.x += WIDTH+1;} 
        });
this.bind("righted", function () { 
        if(this.x > WIDTH)
       { this.x -= WIDTH+1;} });  //seuraa mukana liikuttaessa
        this.append("<b>-=" +this._objectdata.name+"=- LVL" + this._objectdata.LVL +
        "</b><br/>XP:" +this._objectdata.XP +" INT " +this._objectdata.INT +" STR "
        +this._objectdata.STR +" $" +this._objectdata.MONE);
       this.css({ "font-size" : asratio*20 +"px", "cursor" : "hand", "cursor" : "pointer" , "color" : "white", "-moz-user-select" : "none"});
        return this;
    } //egobar
}); // Ajax Get-komponentti

Crafty.c("PostAjax", {
    Ajax: function(address) {
    if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlHttp = new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
//Luodan selaimen mukaan muuttuja xmlhttp-pyynnön iilmentymää varten.
    xmlHttp.open("POST", address, false);
    xmlHttp.send();
} // Ajax-funktio - Post ei palauta mitään joten PlaceJax ja raportti ovat tarpeettomia funktioita.
}); // Ajax Post-komponentti
        Crafty.c("sulje", {
        init: function() {
            this.requires("2D,DOM, Color, Mouse, Image")
            .attr({x: WIDTH-asratio*40, y: asratio*5, w: asratio*30, h: asratio*30, z: 999})
            .color("#996600")
            .image(IMG_X)
            .css({"border-radius" : "5px", "-moz-border-radius" : "5px", "-webkit-border-radius" : "5px",
            "text-align" : "center", "cursor" : "hand", "cursor" : "pointer"})
            .bind("Click", function() {
                if(this.paikka) {
                Crafty.scene(this.paikka);
            }
            else  { 
             Crafty.e("PostAjax")
            .Ajax(this.linkki);
             document.location.href = "http://v-v-2.eu1.frbit.net";
            } // else if
            })
                .bind("lefted", function () { 
                    if(this.x<WIDTH)
                   { this.x += WIDTH+1;} 
                    })

                .bind("righted", function () { 
                    if(this.x > WIDTH)
                   { this.x -= WIDTH+1;} })   //seuraa mukana liikuttaessa

        },
        set: function(paikka) {
            this.paikka = paikka;
            return this;
        }, 
        anchor: function(paikka) {
            this.linkki = paikka;
            return this;
        }
     }); // Crafty-komponentti "sulje ikkuna"-painikkeelle.

        Crafty.c("palkki", {
        init: function() {
            this.requires("2D,DOM, Color")
            .attr({w: WIDTH, h: asratio*26, x: 0, y: 0, z: 10})
            .color("#FFCCCC")
            .css({"border-radius" : "2px", "-moz-border-radius" : "2px", "-webkit-border-radius" : "2px",
                "text-align" : "center"});
        },
        title: function(title) {
            Crafty.e("2D, HTML")
            .replace("<H1>"+ title +"</h1>")
        }
        }); // Crafty-komponentti "ikkunapalkille".

    Crafty.audio.stop();
    Crafty.background("#3d3d3d");
    Crafty.e("sulje")
    .anchor("logout");
    Crafty.e("2D, DOM, Text, Mouse")
    .text("Aloita peli")
    .attr({x: WIDTH/2-asratio*150, y: asratio*10,w: asratio*300, h: asratio*80})
    .css ({"text-align" : "center", "font-size" : asratio*30 +"px", "font-weight" : "bolder", "background-color" : "red",
            "border-radius" :  "20px",
            "-moz-border-radius" : "20px",
            "-webkit-border-radius" : "20px",
            "cursor" : "hand", "cursor" : "pointer" ,
            "webkit-user-select" : "none"})
    .bind("Click", function () {
        //Aloitetaan pelin lataaminen...
    Crafty.scene("loading");
    }); // Aloita peli-nappula
    console.log("true");    
    Crafty.e("2D, DOM, Text, Mouse")
    .text("Luo hahmo")
    .attr({x: WIDTH/2-asratio*150, y: asratio*100,w: asratio*300, h: asratio*80})
    .css ({"text-align" : "center", "font-size" : asratio*30 +"px", "font-weight" : "bolder", "background-color" : "green",
            "border-radius" :  "20px",
            "-moz-border-radius" : "20px",
            "-webkit-border-radius" : "20px",
            "cursor" : "hand", "cursor" : "pointer" ,
            "webkit-user-select" : "none"})

    .bind("Click", function () {
    //Luo hahmo-näkymään...
    Crafty.scene("hahmo");
    }); // Luo hahmo-nappula
    Crafty.e("2D, DOM, Text, Mouse")
    .text("Asetukset")
    .attr({x: WIDTH/2-asratio*150, y: asratio*190,w: asratio*300, h: asratio*80})
    .css ({"text-align" : "center", "font-size" : asratio*30 +"px", "font-weight" : "bolder", "background-color" : "orange",
            "border-radius" :  "20px",
            "-moz-border-radius" : "20px",
            "-webkit-border-radius" : "20px",
            "cursor" : "hand", "cursor" : "pointer" ,
            "webkit-user-select" : "none"})

    .bind("Click", function () {
    //Asetukset-valikkoon...
    Crafty.scene("settings");
    }); // Asetukset-nappula
    Crafty.e("2D, DOM, Text, Mouse")
    .text("Credits")
    .attr({x: WIDTH/2-asratio*150, y: asratio*280,w: asratio*300, h: asratio*80})
    .css ({"text-align" : "center", "font-size" : asratio*30 +"px", "font-weight" : "bolder", "background-color" : "grey",
            "border-radius" :  "20px",
            "-moz-border-radius" : "20px",
            "-webkit-border-radius" : "20px",
            "cursor" : "hand", "cursor" : "pointer" ,
            "webkit-user-select" : "none"})

    .bind("Click", function () {
    //Häpeämätöntä promootiota tässä...
    Crafty.scene("credits");
    }); // Credits-nappula
}); //menu scene päättyy
Crafty.scene("hahmo", function() {
        markup = "<h2>Hahmon luonti</h2>"
            +"<p>Hahmon luonti on tärkeä osa hahmon luontia.</p>"
            +"<p>Kato vaikka Googlesta!</p>"
           +"<a href='http://www.google.com'>Google</a>"
           +"<form method='post' action='user/sam/player'>"
           +"<input type='text' id='name' name='name'>"
           +"<input type='submit' value='OK'>"
           +"</form>";
    Crafty.background("#ffffff");
    console.log("Luo hahmo!");
    Crafty.e("sulje")
    .set("menu");
    Crafty.e("palkki");
    Crafty.e("2D, DOM, Color")
    .attr({x: 10, y: 10, z: 100, w: WIDTH-26, h: HEIGHT-20})
    .css({"border-radius" : "10px", "-moz-border-radius" : "10px",
     "-webkit-border-radius" : "10px", "border" : "2px solid black"})
    .color("#ffffff")
    Crafty.e("2D, HTML, DOM")
    .attr({x: 30, y: 18, w: 400, z: 700})
    .css({"color" : "black"})
    .replace("Vallu ja Valma 2: <b>Hahmon luonti</b>");
    Crafty.e("2D, HTML, DOM")
    .attr({x: 30, y: 68, w: HEIGHT-60, h: 600, z:700})
    .css({"color" : "black", "padding" : "0.0146%"})
    .replace(markup);




}); // Hahmon luonti valikko
Crafty.scene("settings", function() {
        markup = "<h2>Asetukset</h2>"
            +"<p>Muuta täältä pelin asetuksia, kuten <br/>"
            +"<p>viestien näkymistä ja yksityisyysasetuksia.</p>"
           +"<ul><li>Item #1</li><li>Item #2</li><li>Item #3</li></ul>";
    Crafty.background("#ffffff");
    console.log("Aseta asetukset!");
    Crafty.e("sulje")
    .set("menu");
    Crafty.e("palkki");
    Crafty.e("2D, DOM, Color")
    .attr({x: 10, y: 10, z: 120, w: WIDTH, h: HEIGHT})
    .css({"border-radius" : "10px", "-moz-border-radius" : "10px",
     "-webkit-border-radius" : "10px", "border" : "2px solid black"})
    .color("#ffffff")
    Crafty.e("2D, HTML, DOM")
    .attr({x: 30, y: 18, w: 400, z: 99})
    .css({"color" : "black"})
    .replace("Vallu ja Valma 2: <b>Peliasetukset</b>");
    Crafty.e("2D, HTML, DOM")
    .attr({x: 30, y: 68, w: HEIGHT-60, h: 600, z: 199})
    .css({"color" : "black", "padding" : "0.0146%"})
    .replace(markup);
}); // Asetukset-valikko
Crafty.scene("credits", function() {
        markup = "<h2>Kunnia sille, jolle se kuuluu</h2>"
            +"<p>eli minulle ja kaikille muille.</p>"
            +"<p>Kiitokset kaikille ja jos peliä sponsoroidaan,<br/>"
           +"niin tänne logot ja yhteystiedot ja sen sellaset</p>";
    Crafty.background("#ffffff");
    console.log("SPARTANS!");
    Crafty.e("sulje")
    .set("menu");
    Crafty.e("palkki");
    Crafty.e("2D, DOM, Color")
    .attr({x: 10, y: 10, z: 120, w: WIDTH-26, h: HEIGHT-20})
    .css({"border-radius" : "10px", "-moz-border-radius" : "10px",
     "-webkit-border-radius" : "10px", "border" : "2px solid black"})
    .color("#ffffff")
    Crafty.e("2D, HTML, DOM")
    .attr({x: 30, y: 18, w: 400, z: 199})
    .css({"color" : "black"})
    .replace("Vallu ja Valma 2: <b>Making of...</b>");
    Crafty.e("2D, HTML, DOM")
    .attr({x: 30, y: 68, w: HEIGHT-60, h: 600, z: 999})
    .css({"color" : "black", "padding" : "0.0146%"})
    .replace(markup);
}); // Asetukset-valikko
// Määritellään työkkäri näkymä
Crafty.scene("tuokkaris", function() {
    Crafty.viewport.x =0;
    Crafty.e("slide")
    .unsetit();
    Crafty.background("RGB(207, 225, 195)");
    Crafty.e("2D, DOM, Image")
    .setName("entranssi")
    .attr({x: 0, y: 0, w: WIDTH, h: HEIGHT, z: 99})
    .image(IMG_TUOKKARIS);
    Crafty.e("GetAjax")
    .read("");
    console.log(this._objectdata);
    Crafty.e("PostAjax")
    .Ajax("user/sam/add/tuokkari");
    Crafty.e("sulje")
    .set("loading")
    .unbind("lefted")
    .unbind("righted")
    .bind("Click", function () {
    })
    ;
}); // Tämä on työkkäri skene, joka päättyy  'työkäristä' tapahtumaan

Crafty.scene("toissa", function() {
Crafty.viewport.x = 0;
Crafty.background("#F1C18A");
Crafty.e("sulje")
.set("loading");
Crafty.e("2D, HTML, DOM")
.attr({x: 30, y: 118, w: 400, z: 199})
.css({"color" : "black"})
.replace("Tee Töitä! <b>Sinä senkin...</b><br/> <h1>AMMATTILAINEN!</h1>");

}) // Työpaikka skene

Crafty.scene("areena", function() {
Crafty.viewport.x = 0;
Crafty.e("2D, DOM, Color")
.attr({x:0,y:0,w: WIDTH, h: HEIGHT, z:1})
.color("#454500");
        var xp_elem = Crafty.e("2D, DOM, Image, Mouse")
        .attr({x: asratio*50, y: asratio*20, w: asratio*100, h: asratio*30, z: 999})
        .image(IMG_INT)
         .css ({ "cursor" : "hand", "cursor" : "pointer" ,"-webkit-user-select" : "none",
        "-moz-user-select" : "none" })
        .bind("Click",function() {
            Crafty.e("PostAjax")
            .Ajax("user/sam/add/int");
            Crafty.scene("loading");
            
        }); //end bind ->osta Shakki nappi
         Crafty.e("2D, DOM, Image, Mouse")
        .attr({x: asratio*180, y: asratio*20, w: asratio*40, h: asratio*100, z: 999})
        .image(IMG_STR)
         .css ({ "cursor" : "hand", "cursor" : "pointer" ,"-webkit-user-select" : "none",
        "-moz-user-select" : "none" })
        .bind("Click",function() {
            Crafty.e("PostAjax")
            .Ajax("user/sam/add/str");
            Crafty.scene("loading");
            
        }); //osta puntti nappi
Crafty.e("sulje")
.set("loading")
.bind("Click", function() {
Crafty.trigger("rightSide");
});


}) // Areena skene -   bind trigger "RightSide" jotta kuva palaa kohtaan, josta siihen tultiin.

   //Tästä lähtee kaupunki-näkymän alustus
    Crafty.scene("city", function () {

     Crafty.audio.play("BGmusic", -1, 1);  // oletuksena musiikki soi -tauotta ja täysillä .\m/><\m/. VAIN yksi instanssi
    Crafty.viewport.init(WIDTH, HEIGHT); 
    this.bind("RightSide", function () {
    Crafty.viewport.scroll ("_x", -WIDTH);
    });

//HUD


Crafty.e("2D,DOM,Color")
.color("#2a2a2a")
.attr({w: WIDTH, h:asratio*70,x:0,y:0, z: 199, alpha: 0.7 })
.bind("lefted", function () { 
                if(this.x<WIDTH)
               { this.x += WIDTH;} 
                })

.bind("righted", function () { 
                if(this.x === WIDTH)
               { this.x -= WIDTH;} });     
//HUD- käyttäjätiedot
Crafty.e("2D, HTML, GetAjax")
.flush()
.Ajax("user/sam/data")
.attr({w: 300, h:asratio*70,x:0,y:0, z: 998, alpha: 0.7 })
.egobar();



Crafty.e("2D, HTML, GetAjax")
.flush()
.Ajax("user/sam/messages")
.count()
.attr({w: asratio*200, h:asratio*20,x:WIDTH/2,y:0, z: 998, alpha: 0.7 })
.css({"padding-top" : "5px", "font-size" : asratio*17 +"px", "color" : "red",
"padding-left" : "10px", "font-weight" : "bolder", "text-align" : "right" });

Crafty.e("2D, HTML")
.attr({w: 400, h:asratio*70,x:WIDTH/2,y:0, z: 8, alpha: 0.7 })
.bind("lefted", function () { 
    if (this.x==WIDTH/2) { this.x = this.x+WIDTH;}
     })
.bind("righted", function () { 
    if (this.x>0) { this.x = WIDTH/2;} })
.css({"padding-top" : "5px", "font-size" : "10px",
 "padding-left" : "10px", "font-weight" : "bolder", "text-align" : "left" });
/* City-näkymän komponentteja */




Crafty.c("bubble", {
init: function() {
    this.requires("2D, DOM, Mouse, Color")
    .bind("Click", function() {
        Crafty.scene(this.scene);
    })
    .css({   "cursor" : "hand", "cursor" : "pointer" ,
            "border-radius" :  "50px",
            "-moz-border-radius" : "50px",
            "-webkit-border-radius" : "50px"
            })
    .attr( { z: 900, w: asratio*220, h: asratio*100, alpha: 0.97} )
    .bind("Cancel",function () {
    this.destroy();

    a -= 1;
    if(m== 0 && a==0){Crafty.e("koulu");Crafty.e("areena");Crafty.e("tuokkari"); Crafty.e("tuopaikka");}

    }) // no bubbles here, move along!

return this;
   }, // init: function
placexy: function(x,y) {
    if (x<WIDTH)
    {
        if(x>asratio*200 && x<WIDTH-asratio*200)
            {this._x = x-this._w;}
        else {
            this._x = WIDTH/2;
        }
    } // if on the leftside of town
    if (x>WIDTH)
    {
        if(x>WIDTH+asratio*200 && x<WIDTH+(WIDTH*2-asratio*200))
            {this._x = x-this._w;}
        else {
            this._x = WIDTH+(WIDTH/2);
        }
    } //if on the rightside of town

    this._y = y-asratio*40; 

    return this;
},
scene: function(scene) {
    this.scene = scene;
    return this;
}

});//  Crafty.c "bubble"

Crafty.c("bubbleTxt", {
init: function() {
    this.requires("2D, DOM, HTML, Mouse")
    .css({ "-moz-user-select": "none",
        "cursor" : "hand", "cursor" : "pointer",
        "-webkit-user-select": "none",
        "-ms-user-select": "none",
        "text-align" : "left",
        "pointer-events" : "none",
        "font-size": +asratio*17 +"px",
        "color" : "#abc"
        })     
    .attr({ w: asratio*164, h: asratio*64, z: 990})

    .bind("Cancel", function () {
    this.destroy();

    }); //bind Cancel

}, //init
setmsg: function(tarina) {
        this.replace(tarina);
        return this; 
},
placexy: function(x,y) {
    if (x<WIDTH)
    {
        if(x>asratio*200 && x<WIDTH-asratio*200) // border boundaries of 200
            {this._x = x-this._w-asratio*20;}
        else {
            this._x = WIDTH/2+(asratio*20);
        }
    } // if on the leftside of town

    if (x>WIDTH)
    {
        if(x>WIDTH+asratio*200 && x<WIDTH+(WIDTH*2-asratio*200))
            {this._x = x-this._w-asratio*20;}
        else {
            this._x = WIDTH+WIDTH/2+(asratio*20);
        }
    }

        this._y = y-asratio*40;

    return this;
} // if on the rightside of town
 }); //bubbleTxt

Crafty.c("koulu", {


init: function () { 

                Crafty.e("2D, DOM, Image, Mouse")
                .image(IMG_KOULU)
                .setName("koulu")
                .attr({x: WIDTH+asratio*30, y: HEIGHT-asratio*300, z: 200})
                .bind("Click", function(e) {
                Crafty.e("bubble")
                .color("#1166FF")
                .placexy(WIDTH+e.clientX, e.clientY)
                .scene("initGame");
                Crafty.e("bubbleTxt")
                .setmsg("<h1>Koulu</h1>")
                .placexy(WIDTH+e.clientX, e.clientY);
                a += 1;
                Crafty.e("2D,DOM,Color, Mouse")
                .attr({x: WIDTH, y: 0, w: WIDTH, h: HEIGHT, z: 800, alpha: 0.7})
                .color("#161616")
                .bind("Click", function() {
                Crafty.trigger("Cancel");
                this.destroy();
                });
                if(a>0){this.unbind("Click")} //jos joku ikkuna on auki.
                })
                // rakennus-olion bind CLICK

    } //init function

}); // Crafty.c "koulu"

Crafty.c("areena", {


init: function () { 

                Crafty.e("2D, DOM, Image, Mouse")
                .image(IMG_AREENA)
                .setName("areena")
                .attr({x: WIDTH+asratio*430, y: HEIGHT-asratio*300, z: 200})
                .bind("Click", function(e) {
                Crafty.e("bubble")
                .color("#1166FF")
                .placexy(WIDTH+e.clientX, e.clientY)
                .scene("areena");
                Crafty.e("bubbleTxt")
                .setmsg("<h1>Areena</h1>")
                .placexy(WIDTH+e.clientX, e.clientY);
                a += 1;
                Crafty.e("2D,DOM,Color, Mouse")
                .attr({x: WIDTH, y: 0, w: WIDTH, h: HEIGHT, z: 800, alpha: 0.7})
                .color("#161616")
                .bind("Click", function() {
                Crafty.trigger("Cancel");
                this.destroy();
                });
                if(a>0){this.unbind("Click")} //jos joku ikkuna on auki.
                })
                // rakennus-olion bind CLICK

    } //init function

}); // Crafty.c "areena"

Crafty.c("tuokkari", {


init: function () { 

                Crafty.e("2D,DOM, Image, Mouse")
                .image(IMG_TUOKKARI)
                .setName("tuokkari")
                .attr({_x: asratio*20, _y: HEIGHT-asratio*457, z: 200})
                .bind("Click", function(e) {
                 Crafty.e("bubble")
                 .color("#7711EE")
                .placexy(e.clientX+20, e.clientY+10)
                .scene("tuokkaris");
                Crafty.e("bubbleTxt")
                .setmsg("<h1>Työkkäri</h1>")
                .placexy(e.clientX+20,e.clientY+10);
                a += 1;
                Crafty.e("2D,DOM,Color, Mouse")
                .attr({x: 0, y: 0, w: WIDTH, h: HEIGHT, z: 800, alpha: 0.7})
                .color("#161616")
                .bind("Click", function() {
                Crafty.trigger("Cancel");
                this.destroy();
                });
                if(a>0 ){this.unbind("Click")}
                                }) // rakennus-olion bind CLICK
    } //init function

}); // Crafty.c "tuokkari"

Crafty.c("tuopaikka", {


init: function () { 

                Crafty.e("2D,DOM, Image, Mouse")
                .image(IMG_TUOPAIKKA)
                .setName("tuopaikka")
                .attr({_x: asratio*440, _y: HEIGHT-asratio*457, z: 200})
                .bind("Click", function(e) {
                 Crafty.e("bubble")
                 .color("#7711EE")
                .placexy(e.clientX, e.clientY)
                .scene("toissa");
                Crafty.e("bubbleTxt")
                .setmsg("<h1>Töihin</h1>")
                .placexy(e.clientX,e.clientY);
                a += 1;
                Crafty.e("2D,DOM,Color, Mouse")
                .attr({x: 0, y: 0, w: WIDTH, h: HEIGHT, z: 800, alpha: 0.7})
                .color("#161616")
                .bind("Click", function() {
                Crafty.trigger("Cancel");
                this.destroy();
                });
                if(a>0 ){this.unbind("Click")}
                                }) // rakennus-olion bind CLICK
    } //init function

}); // Crafty.c "tuopaikka"

        Crafty.c("slide", {
            init: function() {

                this.requires("2D, DOM");
                this.requires("Keyboard, Mouse");
               /* Not necessarily needed
                var isTouchSupported = 'ontouchstart' in window;
                var startEvent = isTouchSupported ? 'touchstart' : 'mousedown';
                var moveEvent = isTouchSupported ? 'touchmove' : 'mousemove';
                var endEvent = isTouchSupported ? 'touchend' : 'mouseup'; */
                Crafty.addEvent(this, Crafty.stage.elem, 'mousedown', function (e) {
                    if (e.button!=0) return;
                    var base = { x: e.clientX};

                    function scroll(e) {
                        // base = { x: e.clientX};                        
                        var dx = base.x - e.clientX;
                        if (dx >= asratio*200) Crafty.trigger("lefted");
                        else if (dx <= -asratio*200) Crafty.trigger("righted")

                    };

                    Crafty.addEvent(this, Crafty.stage.elem, 'mousemove', scroll);
                    Crafty.addEvent(this, Crafty.stage.elem, 'mouseup', function () {
                    base.x = null;
                    dx = 0;
                    Crafty.removeEvent(this, Crafty.stage.elem, 'mousemove', scroll);
                    
                    });
                }); //mousedown and touchstart - then remove

                this.bind('KeyDown', function () 
                { if (this.isDown('LEFT_ARROW')) { Crafty.trigger("lefted");}
                if (this.isDown('RIGHT_ARROW')) {Crafty.trigger("righted");}
                });                 //keyDown 
            },

            unsetit: function() {
                Crafty.unbind("lefted");
                Crafty.unbind("righted");
            }
        })
        //Maailman rakennus-komponennti
        Crafty.c("CityBase", {

            init: function() {

                 /* Create an Entity for every image.
                 * The "repeat" is essential here as the Entity's width is 3x the canvas width (which equals
                 * the width of the original image).
                 */
  
                Crafty.background("#1155EE");
                Crafty.e("slide");
                this.bgImage = Crafty.e("2D, DOM, Image").image(IMG_SKY)
                    .attr({w: WIDTH*3, x:0, y: asratio*160, z: 3});
                this.weatherImage = Crafty.e("2D, DOM, Image").image(IMG_WTHR)
                    .attr({x:10, y:10,h: 256, w: 256, z: 4});
                this.cloudImage = Crafty.e ("2D, DOM, Image").image(IMG_CLOUD,"repeat")
                    .attr({x:0, y:0, h: asratio*768,w: WIDTH*2,  z: 1});
                this._fgImage = Crafty.e("2D, DOM, Image").image(IMG_GRND, "repeat")
                    .attr({x:0, y:HEIGHT-asratio*260, z: 10, w: WIDTH*2, h:HEIGHT})
                    .bind ("lefted", function () {
                        Crafty.viewport.scroll("_x", -WIDTH);

                    })

                    .bind ("righted", function () {
                        Crafty.viewport.scroll("_x", 0);
                        })
                    .bind("Click", function () {
                        Crafty.trigger("clear");

                    });

            } //Crafty skrolleri
        }); // Crafty component City base komponentti määrrittely loppuu täsä

                Crafty.e("CityBase");
 // komponenttia kutsutaan, eli tehdään siitä entiteetti
                Crafty.e("sulje")
                .set("menu")
                .bind("lefted", function () { 
                    if(this.x<WIDTH)
                   { this.x += WIDTH;} 
                })
            .bind("righted", function () { 
                    if(this.x === WIDTH)
                   { this.x -= WIDTH;} 
               });      // takaisin alkuvalikkoon (TODO) tallennustapahtuma poistuttaessa.
                    Crafty.e("koulu");
                    Crafty.e("areena");
                    Crafty.e("tuokkari");
                    Crafty.e("tuopaikka");
                    Crafty.e("Jukebox");
    }); // Crafty.scene "main" päättyy  täsä
    Crafty.scene("menu"); //Heti alkuun ollaan menussa
}; // window.onload