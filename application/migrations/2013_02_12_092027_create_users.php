<?php

class Create_Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
	Schema::create('users', function($table) {
	    //Primary key - Laravel lukee 'users_id' Eloquentin mukaan
	    $table->increments('id');
	    // varchar 32
	    $table->string('username', 32);
	    $table->string('email', 320);
	    $table->string('password', 64);
	    $table->text('lastlog');
	    // created_at | updated_at DATETIME
	    $table->timestamps();
    });

	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//Sama toisinpäin, eli:
		Schema::drop('users');
	}

}