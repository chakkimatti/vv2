<?php

class Create_Players_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::create('players', function($table) {
	    //Primary key - Laravel lukee 'player_id' Eloquentin/PDO mukaan
	    $table->increments('id');
	    // varchar 32
	    $table->integer('user_id');
	    $table->string('name', 32);
	    $table->integer('LVL')->unsigned();
	    $table->integer('INT');
	    $table->integer('STR');
	    $table->integer('STM');
	    $table->integer('XP')->unsigned();
	    $table->integer('mone');
	    // created_at | updated_at DATETIME
	    $table->timestamps();
    });	}

	/**
	 * Miksi ei MIKÄÄN muutu?
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('players');
	}

}