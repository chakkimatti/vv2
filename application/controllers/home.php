<?php

class Home_Controller extends Base_Controller {

	public $restful = true;
	/*
	|--------------------------------------------------------------------------
	| The Default Controller
	|--------------------------------------------------------------------------
	|
	| Instead of using RESTful routes and anonymous functions, you might wish
	| to use controllers to organize your application API. You'll love them.
	|
	| This controller responds to URIs beginning with "home", and it also
	| serves as the default controller for the application, meaning it
	| handles requests to the root of the application.
	|
	| You can respond to GET requests to "/home/profile" like so:
	|
	|		public function action_profile()
	|		{
	|			return "This is your profile!";
	|		}
	|
	| Any extra segments are passed to the method as parameters:
	|
	|		public function action_profile($id)
	|		{
	|			return "This is the profile for user {$id}.";
	|		}
	|
	*/

	public function get_index()
	{
		return View::make('home.index');
	}

		public function get_reg()
	{
		return View::make('home.rekkaa');
	}

		public function get_welcome()
	{

		if(Auth::user())
		{ 	$sessionid = 123456;
			//$jsond = array("session" => $sessionid);
			Session::put('id', $sessionid);
			return View::make('user.welcome')/*->with('json', $jsond)*/; }
		else if (!Auth::user()) {
			Redirect::to_route('home')->with('message','Session Expiration Documentation Station');
		}
	}	
	
	public function get_ammatit ()
	{
		$xml = path('storage') .'xml/ammatit.xml';
		$ammatit = simplexml_load_file($xml);
		return Response::json($ammatit);
	}
		public function post_login()
	{
		$credentials = array('username' => Input::get('usrname'), 'password' => Input::get('passwd'));

		if (Auth::attempt($credentials))
		{
			
			$user = Auth::user();
			$message = "Tervetuloa takaisin valtsuun!";
		     return Redirect::to_route('welcome')->with('message', $message);
		}

		else
		{
			return Redirect::to_route('home')->with('message', 'Virheellinen käyttäjätunnus tai salasana.');

		}

	}	

		public function post_regit()
	{
		$user = Input::get('usrname');
		$email = Input::get('email');
		$password = Input::get('passwd');
		$lastSeen = date('d.m.Y');
		DB::table('users')->insert(array(
		'username' => $user,
		'password' => Hash::make($password),
		'lastlog' => $lastSeen,
		'email' => $email)
		);
		$credentials = array('username' => $user, 'password' => Input::get('passwd'));

if (Auth::attempt($credentials))
{
	$message = "Tervetuloa valtsuun, " .Auth::user()->username ."!";
	return View::make('user.welcome')->with('message', $message)->with('user', $user );
}
	
	}

	public function post_logout ()
	{

		$name =  Auth::user()->username;
		$message = "Kiitos käynnistä, " .$name ."!";
		Auth::logout();
		return Redirect::to_route('home')->with('message', $message);
	}

	public function get_logout ()
	{
		Auth::logout();
		return Redirect::to_route('home')->with('message','Manual logout - Goodbye!');
	}

}