<?php

class Credential_Controller extends OneAuth\Auth\Controller 
{

    protected function action_error($provider = null, $e = '')
    {
        return View::make('auth.errors', compact('provider', 'e'));
    }
	
}