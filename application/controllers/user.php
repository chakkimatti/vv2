<?php

class User_Controller extends Base_Controller {

public $restful = true;

	public function get_data () 
	{
		$user = Auth::user();
		$player = Player::where('user_id', '=', $user->id)->first();
		$data = array( "name" => $player->name, "LVL" => $player->lvl, "XP" => $player->xp,
		"INT" => $player->int, "STR" => $player->str, "MONE" => $player->mone);
		return Response::json($data);

		
	}

	public function get_messages() {
		$message = Player::find(Auth::user()->id)->messages()->get();
		$message[count($message)] = array( "count" => count($message));
		return Response::json($message);

	}

	public function post_player () 
	{
		$user = Auth::user();
		if(!Player::where('user_id', '=' , $user->id)->first()) {
			$player = new Player();
			$player->user_id = $user->id;
			$player->name = Input::get('name');
			$player->save();
			return Redirect::to_route('welcome')->with('message', "tallennettu onnistuneesti!");
		}
		else {
				return Redirect::to_route('welcome')->with('message', "Sinulla on jo hahmo pelissä!");
		}
		
		
	}
	
	public function post_worked() {
		$player = Player::where('user_id', '=', Auth::user()->id)->first();
		$player->xp += 10;
		$player->save();
	}

	public function post_lvled() {
		$player = Player::where('user_id', '=', Auth::user()->id)->first();
		$player->xp += 10;
		if($player->xp==100)
		{
			$player->xp = 0;
			$player->lvl += 1;
		}
		$player->save();
	}

	public function post_earned() {
		$player = Player::where('user_id', '=', Auth::user()->id)->first();
		$player->mone += 100;
		$player->save();
	}

	public function post_smarted() {
		$player = Player::where('user_id', '=', Auth::user()->id)->first();
		$player->int += 10;
		$player->mone -= 100;
		$player->save();
	}	
	
	public function post_ironed() {
		$player = Player::where('user_id', '=', Auth::user()->id)->first();
		$player->str += 10;
		$player->mone -= 100;
		$player->save();
	}	

}