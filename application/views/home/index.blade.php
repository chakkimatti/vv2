<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>V&amp;V II: The Unemployment Strikes Back</title>
	<meta name="viewport" content="width=device-width">
<link href='http://fonts.googleapis.com/css?family=Ranchers' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Keania+One' rel='stylesheet' type='text/css'>
	{{ HTML::style('laravel/css/style.css') }}
</head>
<body>
	<div class="wrapper">
	@if( Session::has('message') )
	<div id="msg">
		{{ Session::get('message') }} 
	</div>
	@endif

				@if( Auth::guest() )
		<span class="login">
			{{ Form::open_secure('login') }}
			{{ Form::label('usrname', 'Käyttäjänimi') }}
			{{ Form::text('usrname') }}
			{{ Form::label('passwd', 'Salasana') }}
			{{ Form::password('passwd') }}
			{{ Form::submit('Log in') }}
			{{ Form::close() }}
			<a href="{{ URL::to('credential/session/facebook') }}">Kirjaudu facella</a>
			<a href="{{ URL::to_route('reg') }}">Rekisteröidy</a>
		</span>
		@endif
		<div class="clearfix">

			<div class="float">
		<h1>Vallu &amp; Valma 2:</h1>
		<h3>Työttömyyden vastaisku</h3>
		</div>
		</div> <!-- clearfix -->
	</div> <!-- wrapper -->

		<div role="main" class="main">
				<div class="home">
				<ul class="out-links">
					<li><a href="http://www.virtuaamo.fi">Virtuaamon kotisivut</a></li>
					<li><a href="https://twitter.com/virtuaamo">#virtuaamo</a></li>

				</ul>
			</div>
		</div>

</body>
</html>
