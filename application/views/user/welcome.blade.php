<!doctype html>
<html lang="en">
<head>
	<link href='http://fonts.googleapis.com/css?family=Ranchers' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Keania+One' rel='stylesheet' type='text/css'>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>V&amp;V II: The Unemployment Strikes Back</title>
<!-- inside framework -->
	{{ HTML::style('laravel/css/style.css') }}
	{{ HTML::script('js/crafty.js') }}

</head>

<body>
	<div class="wrapper">
	@if( Session::has('message') )
	<div id="msg">
		{{ Session::get('message') }} 
	</div>
	@endif
	{{ HTML::script('js/test2.js')}}

</div> <!-- wrapper ends -->
</body>
</html>
