<?php
class Player extends Eloquent {
	
	public function messages() {
		return $this->has_many('Message', 'player_id');
	}
}