<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

Route::get('/', array( 'as' => 'home', 'uses' => 'home@index' ) ); 
 
Route::get('rekkaa', array( 'as' => 'reg', 'uses' => 'home@reg' )); 

Route::post('login', array( 'as' => 'login', 'uses' => 'home@login' )); 

Route::get('user/sam/player', array( 'uses' => 'user@player') );

Route::get('user/sam/data', array( 'uses' => 'user@data') );

Route::get('user/sam/messages', array( 'uses' => 'user@messages') );

Route::post('user/sam/data', array( 'uses' => 'user@data') );

Route::post('user/sam/player', array( 'uses' => 'user@player') );

Route::post('user/sam/add/tuokkari', array('uses' => 'user@worked'));

Route::post('user/sam/add/xp', array('uses' => 'user@lvled'));

Route::post('user/sam/add/int', array('uses' => 'user@smarted'));

Route::post('user/sam/add/str', array('uses' => 'user@ironed'));

Route::post('user/sam/add/bling', array('uses' => 'user@earned'));

Route::post('regit', array( 'as' => 'regit', 'uses' => 'home@regit' )); 

Route::get('tervetuloa', array('as' => 'welcome', 'uses' => 'home@welcome' ));

Route::post('logout', array('as' => 'logout', 'uses' => 'home@logout' ));

Route::get('logout', array('uses' => 'home@logout' ));


Route::get('ammatit', array ('as' => 'ammatit', 'uses' => 'home@ammatit'));

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Route::get('/', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});